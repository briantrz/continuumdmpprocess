import re, os

dmpFiles = []
files = os.listdir()

for file in files:
  if '.dmp' in file:
    if not 'modified_' in file:
      dmpFiles.append(file)

patterns = [
  {'match' : '((Object : UnitOcc)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '((Object : OccSensor)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '((Object : WarmUp)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '((Object : CoolDown)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '((Object : Purge)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '((Object : AutoReset)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '((Object : ManualReset)((.(?<!EndObject)|\n)+?)(Format : ))(\S*)((.|\n)+?)(EndObject)', 'replace' : r'\1$###\7\9'},
  {'match' : '(?<=Dictionary)(((.(?<!EndDictionary)|\n)+?)(  InfinityDateTime(.)+?: )(\S*) :((.|\n)+?)(UnitStop\n))', 'replace' : r'''\1  InfinityDateTime : \6 : LocalSchedStart : False : LocalSchedStart
  InfinityDateTime : \6 : LocalSchedStop : False : LocalSchedStop
  InfinityDateTime : \6 : AS_FirstDateTime : False : AS_FirstDateTime
  InfinityDateTime : \6 : AS_LastDateTime : False : AS_LastDateTime
  InfinityDateTime : \6 : AS_Sched01Start : False : AS_Sched01Start
  InfinityDateTime : \6 : AS_Sched01Stop : False : AS_Sched01Stop
  InfinityDateTime : \6 : AS_Sched02Start : False : AS_Sched02Start
  InfinityDateTime : \6 : AS_Sched02Stop : False : AS_Sched02Stop
  InfinityNumeric : \6 : LocalSchedOcc : False : LocalSchedOcc
  InfinityNumeric : \6 : HolidayEn : False : HolidayEn
'''},
  {'match' : '(Object : SetSched\n(.(?<!EndObject)|\n)+?DeviceId : ((.)+?)\n(.(?<!EndObject)|\n)+?ByteCode : \n)((.|\n)+?)((EndByteCode)((.|\n)+?)(EndObject))', 'replace' : r'''\1Arg 1 mStart
Arg 2 mStop
Arg 3 mOcc
Arg 4 mOvr
Arg 5 mSched
If (HolidayEn = On and AS_HolidaySnowDay = On) Then
  mStart = AS_FirstDateTime
  mStop = AS_FirstDateTime + 7200
  mOcc = Off
  Return
Endif
If mSched < 0 Then
  mStart = AS_FirstDateTime
  mStop = AS_FirstDateTime + 7200
  mOcc = (mOvr > 0)
  Return
Endif
If mSched = 1 Then
  mStart = AS_Sched01Start
  mStop = AS_Sched01Stop
  mOcc = AS_Sched01Occ or (mOvr > 0)
  Return
Endif
If mSched = 2 Then
  mStart = AS_Sched02Start
  mStop = AS_Sched02Stop
  mOcc = AS_Sched02Occ or (mOvr > 0)
  Return
Endif
If mSched = 3 Then
  mStart = LocalSchedStart
  mStop = LocalSchedStop
  mOcc = LocalSchedOcc or (mOvr > 0)
  Return
Endif
mStart = AS_FirstDateTime
mStop = AS_LastDateTime
mOcc = On
Return
\8\n

 Object : LocalSchedOcc
  Type : InfinityNumeric
  DeviceId : \3
  Alias : LocalSchedOcc
  Format : $###
  Port : 
  Triggers : 
  {
  }
  EndOfCDT
  Value : Off
 EndObject

 Object : HolidayEn
  Type : InfinityNumeric
  DeviceId : \3
  Alias : HolidayEn
  Format : $###
  Port : 
  Triggers : 
  {
  }
  EndOfCDT
  Value : Off
 EndObject
'''}]


for dmpFile in dmpFiles:
  print(dmpFile)
  with open(dmpFile, 'r') as inFile:
    dmpString = inFile.read()
  for pattern in patterns:
    dmpString = re.sub(pattern['match'], pattern['replace'], dmpString)

  outFile = open('modified_'+dmpFile, 'w')
  outFile.write(dmpString)
  outFile.close()